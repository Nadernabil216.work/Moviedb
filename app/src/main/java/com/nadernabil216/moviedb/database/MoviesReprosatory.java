package com.nadernabil216.moviedb.database;

import android.arch.lifecycle.LiveData;
import android.content.Context;
import android.os.AsyncTask;

import com.nadernabil216.moviedb.models.objects.Movie;

import java.util.ArrayList;
import java.util.List;

public class MoviesReprosatory {
    private MovieDao dao;
    private Context context;

    public MoviesReprosatory(Context context) {
        this.context = context;
        MovieDataBase dataBase = MovieDataBase.getINSTANCE(context);
        dao = dataBase.movieDao();
    }

    public List<Movie> getData() {
        return dao.getAllMovies();
    }

    public void insert(Movie movie) {
        new insertAsyncTask(dao).execute(movie);
    }

    public void insertAllData(List<Movie> newMovieList) {
        Movie movie = dao.getAnyMovie();
        if (movie == null) {
            new insertAllDataAsyncTask(dao).execute(newMovieList);
        } else {
            List<Movie> moviesToInsertList = new ArrayList<>();
            List<Movie> allSavedMovies = dao.getAllMovies();
            for (Movie m : newMovieList) {
                if (!allSavedMovies.contains(m)) {
                    moviesToInsertList.add(m);
                }
            }
            new insertAllDataAsyncTask(dao).execute(moviesToInsertList);
        }

    }

    public void setMovieAsFavourite(Movie movie) {
        dao.setMovieAsFavourite(movie);
    }

    public List<Movie> getFavouriteMovies() {
        return dao.getFavouriteMovies(true);
    }

    public LiveData<Movie> getMovie(int movieId) {
        return dao.getMovie(movieId);
    }

    public void delete() {
        dao.deleteAll();
    }

    private static class insertAsyncTask extends AsyncTask<Movie, Void, Void> {

        private MovieDao mAsyncTaskDao;

        insertAsyncTask(MovieDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Movie... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }
    }

    private static class insertAllDataAsyncTask extends AsyncTask<List<Movie>, Void, Void> {

        private MovieDao mAsyncTaskDao;

        insertAllDataAsyncTask(MovieDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(List<Movie>... lists) {
            mAsyncTaskDao.insertAllData(lists[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }
    }


}
