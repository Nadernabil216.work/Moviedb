package com.nadernabil216.moviedb.database;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.nadernabil216.moviedb.models.objects.Movie;

import java.util.List;

@Dao
public interface MovieDao {

    @Insert
    void insert(Movie movie);

    @Insert
    void insertAllData(List<Movie> movieList);

    @Query("SELECT * FROM movie_table LIMIT 1")
    Movie getAnyMovie();

    @Query("SELECT * FROM movie_table")
    List<Movie> getAllMovies();

    @Update
    void setMovieAsFavourite(Movie movie);

    @Query("SELECT * FROM movie_table WHERE userFavourite = :isFavourite")
    List<Movie> getFavouriteMovies(boolean isFavourite );

    @Query("SELECT * FROM movie_table WHERE movie_id = :movieId")
    LiveData<Movie> getMovie(int movieId);

    @Query("DELETE FROM movie_table WHERE movie_id = :movieId")
    void deleteMovie(int movieId);

    @Query("DELETE FROM movie_table")
    void deleteAll();


}
