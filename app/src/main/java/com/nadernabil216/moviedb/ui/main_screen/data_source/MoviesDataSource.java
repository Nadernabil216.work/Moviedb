package com.nadernabil216.moviedb.ui.main_screen.data_source;

import android.arch.paging.PageKeyedDataSource;
import android.support.annotation.NonNull;

import com.nadernabil216.moviedb.database.MoviesReprosatory;
import com.nadernabil216.moviedb.models.objects.Movie;
import com.nadernabil216.moviedb.models.responses.PopularMoviesResponse;
import com.nadernabil216.moviedb.rest.RetrofitClient;
import com.nadernabil216.moviedb.utils.AppController;
import com.nadernabil216.moviedb.utils.Constants;

import retrofit2.Response;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class MoviesDataSource extends PageKeyedDataSource<Integer, Movie> {


    //the size of a page that we want
    public static final int PAGE_SIZE = 20;

    //we will start from the first page which is 1
    private static final int FIRST_PAGE = 1;

    public MoviesDataSource(MoviesReprosatory moviesReprosatory) {
        this.moviesReprosatory = moviesReprosatory;
    }

    private MoviesReprosatory moviesReprosatory  ;

    @Override
    public void loadInitial(@NonNull LoadInitialParams<Integer> params, @NonNull LoadInitialCallback<Integer, Movie> callback) {
        Observable<Response<PopularMoviesResponse>> call = RetrofitClient.getInstance().getApi().getMovies(Constants.ConstantsKeys.apiKey, FIRST_PAGE, "en-US");
        call.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<PopularMoviesResponse>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Response<PopularMoviesResponse> response) {
                        if (response.body() != null) {
                            callback.onResult(response.body().getMovies(), null, FIRST_PAGE + 1);
                            moviesReprosatory.insertAllData(response.body().getMovies());
                        }
                    }
                });
    }

    @Override
    public void loadBefore(@NonNull LoadParams<Integer> params, @NonNull LoadCallback<Integer, Movie> callback) {
        Observable<Response<PopularMoviesResponse>> call = RetrofitClient.getInstance().getApi().getMovies(Constants.ConstantsKeys.apiKey, params.key, "en-US");
        call.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<PopularMoviesResponse>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Response<PopularMoviesResponse> response) {
                        Integer adjacentKey = (params.key > 1) ? params.key - 1 : null;
                        if (response.body() != null) {
                            callback.onResult(response.body().getMovies(), adjacentKey);
                        }
                    }
                });

    }

    @Override
    public void loadAfter(@NonNull LoadParams<Integer> params, @NonNull LoadCallback<Integer, Movie> callback) {
        Observable<Response<PopularMoviesResponse>> call = RetrofitClient.getInstance().getApi().getMovies(Constants.ConstantsKeys.apiKey, params.key, "en-US");
        call.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<PopularMoviesResponse>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Response<PopularMoviesResponse> response) {
                        if (response.body() != null) {
                            Integer key = response.body().getPage() + 1;
                            callback.onResult(response.body().getMovies(), key);
                            moviesReprosatory.insertAllData(response.body().getMovies());
                        }
                    }
                });
    }
}

