package com.nadernabil216.moviedb.ui.movie_details_screen.view_model;

import android.app.Application;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.Observer;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.nadernabil216.moviedb.R;
import com.nadernabil216.moviedb.base.BaseCallBack;
import com.nadernabil216.moviedb.base.BaseViewModel;
import com.nadernabil216.moviedb.database.MoviesReprosatory;
import com.nadernabil216.moviedb.databinding.ActivityMovieDetailsBinding;
import com.nadernabil216.moviedb.models.objects.Movie;
import com.nadernabil216.moviedb.utils.Constants;
import com.squareup.picasso.Picasso;

public class MovieDetailsViewModel extends BaseViewModel {
    private ActivityMovieDetailsBinding binding;
    private BaseCallBack callBack;
    private Context context;
    private MoviesReprosatory moviesReprosatory;
    private Movie movie;

    public MovieDetailsViewModel(@NonNull Application application) {
        super(application);
    }

    public void setBinding(ActivityMovieDetailsBinding binding) {
        this.binding = binding;
    }

    public void setCallBack(BaseCallBack callBack) {
        this.callBack = callBack;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void setData() {
        moviesReprosatory = new MoviesReprosatory(context);
        moviesReprosatory.getMovie(movie.getId()).observe((LifecycleOwner) context, new Observer<Movie>() {
            @Override
            public void onChanged(@Nullable Movie movieDb) {
                movie=movieDb;
                binding.tvOverview.setText(movieDb.getOverview());
                Picasso.with(context).load(Constants.ConstantsKeys.posterBaseUrl + movieDb.getPosterPath()).placeholder(R.drawable.image_placeholder).into(binding.ivPoster);
                if (movieDb.isFavourite()) {
                    binding.btnFavourite.setText("Remove From Favourite");
                } else {
                    binding.btnFavourite.setText("Add To Favourite");
                }
            }
        });

    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public View.OnClickListener changeMovieFavouriteState() {
        return v -> {
            if (movie.isFavourite()) {
                movie.setFavourite(false);
                callBack.showSnackBar("Removed From Favourites");
            } else {
                movie.setFavourite(true);
                callBack.showSnackBar("Added to Favourites");
            }
            moviesReprosatory.setMovieAsFavourite(movie);
        };
    }
}
