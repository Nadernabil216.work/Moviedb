package com.nadernabil216.moviedb.ui.movie_details_screen.view;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.andrognito.flashbar.Flashbar;
import com.google.gson.Gson;
import com.nadernabil216.moviedb.R;
import com.nadernabil216.moviedb.base.BaseCallBack;
import com.nadernabil216.moviedb.databinding.ActivityMovieDetailsBinding;
import com.nadernabil216.moviedb.models.objects.Movie;
import com.nadernabil216.moviedb.ui.movie_details_screen.view_model.MovieDetailsViewModel;
import com.nadernabil216.moviedb.utils.Constants;

import java.util.Arrays;

public class MovieDetailsActivity extends AppCompatActivity implements BaseCallBack {
    private ActivityMovieDetailsBinding binding;
    private MovieDetailsViewModel viewModel;
    private Movie movie;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initialization();
    }

    @Override
    public void initialization() {
        binding=DataBindingUtil.setContentView(this,R.layout.activity_movie_details);
        viewModel = ViewModelProviders.of(this).get(MovieDetailsViewModel.class);
        binding.setVm(viewModel);
        viewModel.setBinding(binding);
        viewModel.setCallBack(this);
        viewModel.setContext(this);

        Intent intent = getIntent();
        String movieStr = intent.getStringExtra(Constants.IntentKeys.movie);
        movie=new Gson().fromJson(movieStr, Movie.class);
        getSupportActionBar().setTitle(movie.getTitle());
        viewModel.setMovie(movie);
        viewModel.setData();
    }

    @Override
    public void showSnackBar(String msg) {
        new Flashbar.Builder(this)
                .title(R.string.alert)
                .message(msg)
                .enableSwipeToDismiss()
                .gravity(Flashbar.Gravity.BOTTOM)
                .messageSizeInSp(16f)
                .showIcon()
                .duration(1000)
                .show();

    }

    @Override
    public void showProgressDialog(boolean showProgress) {
    }

    @Override
    public void showSnakeBarWithActionToFinishActivity(String msg) {

    }

    @Override
    public void finishActivity() {
        finish();
    }

    @Override
    public void onBackPressed() {
        finishActivity();
    }
}
