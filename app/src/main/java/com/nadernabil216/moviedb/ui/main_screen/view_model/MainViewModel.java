package com.nadernabil216.moviedb.ui.main_screen.view_model;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.arch.paging.LivePagedListBuilder;
import android.arch.paging.PageKeyedDataSource;
import android.arch.paging.PagedList;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.nadernabil216.moviedb.base.BaseViewModel;
import com.nadernabil216.moviedb.callBacks.MainScreenCallBack;
import com.nadernabil216.moviedb.database.MoviesReprosatory;
import com.nadernabil216.moviedb.models.objects.Movie;
import com.nadernabil216.moviedb.ui.login_screen.view.LoginActivity;
import com.nadernabil216.moviedb.ui.main_screen.data_source.MoviesDataSource;
import com.nadernabil216.moviedb.ui.main_screen.data_source.MoviesDataSourceFactory;
import com.nadernabil216.moviedb.ui.main_screen.view.MainActivity;
import com.nadernabil216.moviedb.utils.GeneralMethods;
import com.nadernabil216.moviedb.utils.SharedPreferencesManager;

import java.util.ArrayList;
import java.util.List;

public class MainViewModel extends BaseViewModel {
    //creating livedata for PagedList and PagedKeyedDataSource
    public LiveData<PagedList<Movie>> itemPagedList;
    public LiveData<PageKeyedDataSource<Integer, Movie>> liveDataSource;
    private MoviesReprosatory moviesReprosatory ;
    private MainScreenCallBack mainScreenCallBack ;

    public MainViewModel(@NonNull Application application) {
        super(application);
    }

    public void setMainScreenCallBack(MainScreenCallBack mainScreenCallBack) {
        this.mainScreenCallBack = mainScreenCallBack;
    }

    public void checkInternetConnectionAndGetData(Context context){
        moviesReprosatory = new MoviesReprosatory(getApplication());
        if (GeneralMethods.getInstance().isNetworkAvailable(context)){
            getOnlineData();
            mainScreenCallBack.setOnLineData();
        }else {
            getOfflineData(context);
        }
    }

    private void getOnlineData() {
        //getting our data source factory
        MoviesDataSourceFactory dataSourceFactory = new MoviesDataSourceFactory(moviesReprosatory);

        //getting the live data source from data source factory
        liveDataSource = dataSourceFactory.getItemLiveDataSource();

        //Getting PagedList config
        PagedList.Config pagedListConfig =
                (new PagedList.Config.Builder())
                        .setEnablePlaceholders(true)
                        .setPageSize(MoviesDataSource.PAGE_SIZE).build();
        //Building the paged list
        itemPagedList = (new LivePagedListBuilder(dataSourceFactory, pagedListConfig))
                .build();

    }

    private void getOfflineData(Context context) {
        mainScreenCallBack.setOfflineData((ArrayList<Movie>) moviesReprosatory.getData());
    }

    public void logOut(Context context){
        moviesReprosatory.delete();
        SharedPreferencesManager.getInstance().clearUserData();
        com.jaychang.sa.facebook.SimpleAuth.disconnectFacebook();
        context.startActivity(new Intent(context,LoginActivity.class));
    }
}
