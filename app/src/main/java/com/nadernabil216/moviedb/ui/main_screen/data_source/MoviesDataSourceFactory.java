package com.nadernabil216.moviedb.ui.main_screen.data_source;

import android.arch.lifecycle.MutableLiveData;
import android.arch.paging.DataSource;
import android.arch.paging.PageKeyedDataSource;

import com.nadernabil216.moviedb.database.MoviesReprosatory;
import com.nadernabil216.moviedb.models.objects.Movie;

public class MoviesDataSourceFactory extends DataSource.Factory {


    //creating the mutable live data
    private MutableLiveData<PageKeyedDataSource<Integer, Movie>> itemLiveDataSource = new MutableLiveData<>();

    private MoviesReprosatory moviesReprosatory ;

    public MoviesDataSourceFactory(MoviesReprosatory moviesReprosatory) {
        this.moviesReprosatory = moviesReprosatory;
    }

    @Override
    public DataSource<Integer, Movie> create() {
        //getting our data source object
        MoviesDataSource dataSource = new MoviesDataSource(moviesReprosatory);


        //posting the datasource to get the values
        itemLiveDataSource.postValue(dataSource);

        //returning the datasource
        return dataSource;
    }


    //getter for itemlivedatasource
    public MutableLiveData<PageKeyedDataSource<Integer, Movie>> getItemLiveDataSource() {
        return itemLiveDataSource;
    }
}

