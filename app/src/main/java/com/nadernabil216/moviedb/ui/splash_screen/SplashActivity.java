package com.nadernabil216.moviedb.ui.splash_screen;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.easyandroidanimations.library.FadeInAnimation;
import com.nadernabil216.moviedb.R;
import com.nadernabil216.moviedb.ui.login_screen.view.LoginActivity;
import com.nadernabil216.moviedb.ui.main_screen.view.MainActivity;
import com.nadernabil216.moviedb.utils.SharedPreferencesManager;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        ImageView ivLogo = findViewById(R.id.ivLogo);
        new FadeInAnimation(ivLogo).setDuration(1500).setListener(animation -> {
            SharedPreferencesManager.getInstance().initialize(this);

            if (SharedPreferencesManager.getInstance().isLoggedIn()) {
                startActivity(new Intent(SplashActivity.this, MainActivity.class));
            } else {
                startActivity(new Intent(SplashActivity.this, LoginActivity.class));
            }
        }).animate();
    }
}
