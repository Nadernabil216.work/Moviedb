package com.nadernabil216.moviedb.ui.favourites_screen.view_model;

import android.app.Application;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.Observer;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.nadernabil216.moviedb.base.BaseViewModel;
import com.nadernabil216.moviedb.callBacks.FavouritesCallBack;
import com.nadernabil216.moviedb.database.MoviesReprosatory;
import com.nadernabil216.moviedb.models.objects.Movie;

import java.util.ArrayList;
import java.util.List;

public class FavouritesViewModel extends BaseViewModel {
private FavouritesCallBack callBack;
private MoviesReprosatory moviesReprosatory ;
private Context context ;

    public FavouritesViewModel(@NonNull Application application) {
        super(application);
    }

    public void setCallBack(FavouritesCallBack callBack) {
        this.callBack = callBack;
    }

    public void setMoviesReprosatory(MoviesReprosatory moviesReprosatory) {
        this.moviesReprosatory = moviesReprosatory;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void getData(){
        moviesReprosatory=new MoviesReprosatory(context);
        callBack.setData((ArrayList<Movie>) moviesReprosatory.getFavouriteMovies());
    }
}
