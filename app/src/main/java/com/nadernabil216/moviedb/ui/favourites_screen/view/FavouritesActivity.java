package com.nadernabil216.moviedb.ui.favourites_screen.view;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.nadernabil216.moviedb.R;
import com.nadernabil216.moviedb.adapters.OfflineMoviesAdapter;
import com.nadernabil216.moviedb.callBacks.FavouritesCallBack;
import com.nadernabil216.moviedb.databinding.ActivityFavouritesBinding;
import com.nadernabil216.moviedb.models.objects.Movie;
import com.nadernabil216.moviedb.ui.favourites_screen.view_model.FavouritesViewModel;
import com.nadernabil216.moviedb.utils.GridSpacingItemDecoration;

import java.util.ArrayList;

public class FavouritesActivity extends AppCompatActivity implements FavouritesCallBack {
    private ActivityFavouritesBinding binding;
    private FavouritesViewModel viewModel;
    private OfflineMoviesAdapter offlineMoviesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initialization();
        viewModel.getData();
    }

    private void initialization() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_favourites);
        viewModel = ViewModelProviders.of(this).get(FavouritesViewModel.class);
        viewModel.setCallBack(this);
        viewModel.setContext(this);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 3);
        binding.rvMovies.addItemDecoration(new GridSpacingItemDecoration(3, GridSpacingItemDecoration.dpToPx(10, this), true));
        binding.rvMovies.setLayoutManager(mLayoutManager);
        binding.rvMovies.setItemAnimator(new DefaultItemAnimator());

        getSupportActionBar().setTitle("Favourites");
    }

    @Override
    public void setData(ArrayList<Movie> movies) {
        offlineMoviesAdapter = new OfflineMoviesAdapter(movies, this);
        binding.rvMovies.setAdapter(offlineMoviesAdapter);
    }
}
