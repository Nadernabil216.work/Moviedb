package com.nadernabil216.moviedb.ui.login_screen.view;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.andrognito.flashbar.Flashbar;
import com.nadernabil216.moviedb.R;
import com.nadernabil216.moviedb.base.BaseCallBack;
import com.nadernabil216.moviedb.databinding.ActivityLoginBinding;
import com.nadernabil216.moviedb.ui.login_screen.view_model.LoginViewModel;

public class LoginActivity extends AppCompatActivity implements BaseCallBack {
    private ActivityLoginBinding binding;
    private LoginViewModel viewModel;
    private Flashbar progressFlashBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initialization();
    }

    @Override
    public void initialization() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        viewModel = ViewModelProviders.of(this).get(LoginViewModel.class);
        binding.setVm(viewModel);
        viewModel.setBinding(binding);
        viewModel.setCallBack(this);
        viewModel.setContext(this);
    }

    @Override
    public void showSnackBar(String msg) {
        new Flashbar.Builder(this)
                .title(R.string.alert)
                .message(msg)
                .enableSwipeToDismiss()
                .gravity(Flashbar.Gravity.BOTTOM)
                .messageSizeInSp(16f)
                .showIcon()
                .duration(5000)
                .show();

    }

    @Override
    public void showProgressDialog(boolean showProgress) {
        if (showProgress) {
            progressFlashBar = new Flashbar.Builder(this)
                    .gravity(Flashbar.Gravity.BOTTOM)
                    .title(R.string.signing_in)
                    .message(R.string.please_wait)
                    .backgroundColorRes(R.color.colorPrimaryDark)
                    .showProgress(Flashbar.ProgressPosition.RIGHT)
                    .progressTintRes(R.color.white)
                    .build();
            progressFlashBar.show();
        } else {
            progressFlashBar.dismiss();
        }

    }

    @Override
    public void showSnakeBarWithActionToFinishActivity(String msg) {

    }

    @Override
    public void finishActivity() {
        finishAffinity();
    }

    @Override
    public void onBackPressed() {
        finishActivity();
    }
}
