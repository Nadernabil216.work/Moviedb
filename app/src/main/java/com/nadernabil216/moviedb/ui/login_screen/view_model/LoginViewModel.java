package com.nadernabil216.moviedb.ui.login_screen.view_model;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.view.View;

import com.jaychang.sa.AuthCallback;
import com.jaychang.sa.SocialUser;
import com.nadernabil216.moviedb.R;
import com.nadernabil216.moviedb.base.BaseCallBack;
import com.nadernabil216.moviedb.base.BaseViewModel;
import com.nadernabil216.moviedb.databinding.ActivityLoginBinding;
import com.nadernabil216.moviedb.ui.main_screen.view.MainActivity;
import com.nadernabil216.moviedb.utils.SharedPreferencesManager;

import java.util.Arrays;
import java.util.List;

public class LoginViewModel extends BaseViewModel {
    private ActivityLoginBinding binding ;
    private Context context ;
    private BaseCallBack callBack ;

    public LoginViewModel(@NonNull Application application) {
        super(application);
    }

    public void setBinding(ActivityLoginBinding binding) {
        this.binding = binding;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void setCallBack(BaseCallBack callBack) {
        this.callBack = callBack;
    }

    public View.OnClickListener onButtonLoginClick(){
        return v ->{
            callBack.showProgressDialog(true);
            startFaceBookAuth();
        };
    }

    private void startFaceBookAuth() {
        List<String> scopes = Arrays.asList("user_birthday", "user_friends");
        com.jaychang.sa.facebook.SimpleAuth.connectFacebook(scopes, new AuthCallback() {
            @Override
            public void onSuccess(SocialUser socialUser) {
                callBack.showProgressDialog(false);
                SharedPreferencesManager.getInstance().setUserLogged(true);
                SharedPreferencesManager.getInstance().setUserId(socialUser.userId);
                context.startActivity(new Intent(context,MainActivity.class));
            }

            @Override
            public void onError(Throwable error) {
                callBack.showProgressDialog(false);
                callBack.showSnackBar(error.getLocalizedMessage());
            }

            @Override
            public void onCancel() {
                callBack.showProgressDialog(false);
                callBack.showSnackBar("Error while proceeding login");
            }
        });
    }

}
