package com.nadernabil216.moviedb.ui.main_screen.view;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.andrognito.flashbar.Flashbar;
import com.nadernabil216.moviedb.R;
import com.nadernabil216.moviedb.adapters.OfflineMoviesAdapter;
import com.nadernabil216.moviedb.adapters.OnLineMoviesAdapter;
import com.nadernabil216.moviedb.callBacks.MainScreenCallBack;
import com.nadernabil216.moviedb.databinding.ActivityMainBinding;
import com.nadernabil216.moviedb.models.objects.Movie;
import com.nadernabil216.moviedb.ui.favourites_screen.view.FavouritesActivity;
import com.nadernabil216.moviedb.ui.main_screen.view_model.MainViewModel;
import com.nadernabil216.moviedb.utils.GridSpacingItemDecoration;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements MainScreenCallBack {
    private Flashbar progressFlashBar;
    private OnLineMoviesAdapter onLineMoviesAdapter;
    private OfflineMoviesAdapter offlineMoviesAdapter;
    private ActivityMainBinding binding;
    private MainViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initialization();
        viewModel.checkInternetConnectionAndGetData(this);
    }

    @Override
    public void initialization() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        viewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        viewModel.setMainScreenCallBack(this);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 3);
        binding.rvMovies.addItemDecoration(new GridSpacingItemDecoration(3, GridSpacingItemDecoration.dpToPx(10, this), true));
        binding.rvMovies.setLayoutManager(mLayoutManager);
        binding.rvMovies.setItemAnimator(new DefaultItemAnimator());

    }

    @Override
    public void setOnLineData() {
        showProgressDialog(true);
        onLineMoviesAdapter = new OnLineMoviesAdapter(this);
        binding.rvMovies.setAdapter(onLineMoviesAdapter);
        viewModel.itemPagedList.observe(this, movies -> {
            onLineMoviesAdapter.submitList(movies);
        });
    }

    @Override
    public void setOfflineData(ArrayList<Movie> movies) {
        offlineMoviesAdapter = new OfflineMoviesAdapter(movies, this);
        binding.rvMovies.setAdapter(offlineMoviesAdapter);
    }


    @Override
    public void showSnackBar(String msg) {
        new Flashbar.Builder(this)
                .title(R.string.alert)
                .message(msg)
                .enableSwipeToDismiss()
                .gravity(Flashbar.Gravity.BOTTOM)
                .messageSizeInSp(16f)
                .showIcon()
                .duration(5000)
                .show();

    }

    @Override
    public void showProgressDialog(boolean showProgress) {
        if (showProgress) {
            progressFlashBar = new Flashbar.Builder(this)
                    .gravity(Flashbar.Gravity.BOTTOM)
                    .title(R.string.gettingData)
                    .message(R.string.please_wait)
                    .backgroundColorRes(R.color.colorPrimaryDark)
                    .showProgress(Flashbar.ProgressPosition.RIGHT)
                    .progressTintRes(R.color.white)
                    .duration(3000)
                    .build();
            progressFlashBar.show();
        } else {
            progressFlashBar.dismiss();
        }

    }

    @Override
    public void showSnakeBarWithActionToFinishActivity(String msg) {

    }

    @Override
    public void finishActivity() {
        finishAffinity();
    }

    @Override
    public void onBackPressed() {
        finishActivity();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_favourites) {
            startActivity(new Intent(this,FavouritesActivity.class));
            return true;
        }else {
            viewModel.logOut(this);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }
}
