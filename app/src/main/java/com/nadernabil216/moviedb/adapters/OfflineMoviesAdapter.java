package com.nadernabil216.moviedb.adapters;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.nadernabil216.moviedb.R;
import com.nadernabil216.moviedb.databinding.ItemMovieBinding;
import com.nadernabil216.moviedb.models.objects.Movie;
import com.nadernabil216.moviedb.ui.movie_details_screen.view.MovieDetailsActivity;
import com.nadernabil216.moviedb.utils.Constants;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class OfflineMoviesAdapter extends RecyclerView.Adapter<OfflineMoviesAdapter.ViewHolder> {
    private ArrayList<Movie> movies;
    private Context context;

    public OfflineMoviesAdapter(ArrayList<Movie> movies, Context context) {
        this.movies = movies;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(context);
        ItemMovieBinding binding = DataBindingUtil.inflate(inflater, R.layout.item_movie, viewGroup, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        Movie movie = movies.get(i);
        viewHolder.binding.tvTitle.setText(movie.getTitle());
        Picasso.with(context).load(Constants.ConstantsKeys.posterBaseUrl + movie.getPosterPath()).placeholder(R.drawable.image_placeholder).into(viewHolder.binding.ivMoviePoster);
        viewHolder.itemView.setOnClickListener(view -> {
            Intent intent = new Intent(context, MovieDetailsActivity.class);
            intent.putExtra(Constants.IntentKeys.movie, new Gson().toJson(movie));
            context.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ItemMovieBinding binding;

        public ViewHolder(ItemMovieBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
