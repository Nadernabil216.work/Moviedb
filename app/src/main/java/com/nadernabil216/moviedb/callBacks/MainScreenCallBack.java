package com.nadernabil216.moviedb.callBacks;

import com.nadernabil216.moviedb.base.BaseCallBack;
import com.nadernabil216.moviedb.models.objects.Movie;

import java.util.ArrayList;

public interface MainScreenCallBack extends BaseCallBack {

    void setOnLineData();

    void setOfflineData(ArrayList<Movie> movies);
}
