package com.nadernabil216.moviedb.callBacks;

import com.nadernabil216.moviedb.models.objects.Movie;

import java.util.ArrayList;

public interface FavouritesCallBack {
    void setData(ArrayList<Movie> movies);
}
