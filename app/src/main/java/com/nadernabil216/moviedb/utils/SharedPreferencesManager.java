package com.nadernabil216.moviedb.utils;

import android.content.Context;

import com.orhanobut.hawk.Hawk;

public class SharedPreferencesManager {

    private static final SharedPreferencesManager ourInstance = new SharedPreferencesManager();

    public static SharedPreferencesManager getInstance() {
        return ourInstance;
    }

    private SharedPreferencesManager() {
    }

    public SharedPreferencesManager initialize(Context context) {
        Hawk.init(context).build();
        return ourInstance;
    }

    public boolean isLoggedIn() {
        return Hawk.get(Constants.SharedPrefKeys.isLogged, false);
    }

    public void setUserLogged(boolean userLogged) {
        Hawk.put(Constants.SharedPrefKeys.isLogged, userLogged);
    }

    public String getUserId() {
        return Hawk.get(Constants.SharedPrefKeys.userId, "");
    }

    public void setUserId(String userID) {
        Hawk.put(Constants.SharedPrefKeys.userId, userID);
    }

    public void clearUserData() {
        Hawk.deleteAll();
    }



}
