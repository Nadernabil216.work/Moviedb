package com.nadernabil216.moviedb.utils;

public class Constants {
    public static class IntentKeys {
        public static final String movie = "movie";
    }

    public static class SharedPrefKeys {
        public static final String isLogged = "isLogged";
        public static final String userId = "userId";
    }

    public static class ConstantsKeys {
        public static final String posterBaseUrl = "http://image.tmdb.org/t/p/w780/";
        public static final String apiKey = "1754a73f19a74092e491b0ed39a14fdd";
    }
}
